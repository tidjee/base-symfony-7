## Contribuer à Base Symfony 7

[English version here](./docs/CONTRIBUTING-en.md)

<!-- @import "[TOC]" {cmd="toc" depthFrom=3 depthTo=6 orderedList=false} -->

### Table des matières

<!-- code_chunk_output -->

- [Table des matières](#table-des-matières)
- [Comment contribuer](#comment-contribuer)
- [Conventions de contribution](#conventions-de-contribution)
- [Processus de soumission des modifications](#processus-de-soumission-des-modifications)
- [Directives de style de codage](#directives-de-style-de-codage)
- [Contribuer encore plus](#contribuer-encore-plus)
- [Rôles et responsabilités](#rôles-et-responsabilités)
- [Licence](#licence)
- [Respecter les autres contributeurs](#respecter-les-autres-contributeurs)

<!-- /code_chunk_output -->

Nous vous encourageons à contribuer à ce projet en suivant ces directives.

### Comment contribuer

1. **Créez un compte GitLab si vous n'en avez pas déjà un.**
2. **Créez un fork de ce dépôt.**
3. **Faites vos modifications.**
4. **Poussez vos modifications vers votre fork.**
5. **Créez une merge request (MR) depuis votre fork vers le dépôt principal.**
6. **Attendez que les responsables du projet examinent votre MR.**
7. **Répondez aux commentaires et apportez les modifications demandées.**
8. **Votre MR sera fusionnée dans le dépôt principal une fois approuvée.**

### Conventions de contribution

- Utilisez des commits descriptifs et significatifs.
- Suivez les normes de codage PSR-2.
- Rédigez des tests unitaires pour vos modifications.
- Documentez votre code clairement.
- Respectez les autres contributeurs.

### Processus de soumission des modifications

1. **Créez une branche pour chaque nouvelle fonctionnalité ou correction de bug.**
2. **Commettez vos modifications de manière atomique.**
3. **Poussez votre branche vers votre fork.**
4. **Créez une MR à partir de votre branche vers le dépôt principal.**
5. **Donnez un titre clair et une description détaillée à votre MR.**
6. **Répondez aux commentaires et apportez les modifications demandées.**
7. **Votre MR sera fusionnée dans le dépôt principal une fois approuvée.**

### Directives de style de codage

- Suivez les normes de codage PSR-2.
- Utilisez des espaces de 4 caractères pour l'indentation.
- Utilisez des noms de variables et de fonctions descriptifs.
- Ajoutez des commentaires à votre code pour expliquer sa fonction.

### Contribuer encore plus

- Signaler des bugs et proposer des améliorations dans l'issue tracker.
- Aider les autres contributeurs en répondant aux questions sur le forum ou sur le chat.
- Documenter le projet en améliorant le fichier README.md et d'autres documents.
- Traduire le projet dans d'autres langues.

### Rôles et responsabilités

- **Maintainers**: Les responsables du projet sont chargés de fusionner les merge requests, de publier des versions et de guider la direction du projet.
- **Contributeurs**: Les contributeurs sont des développeurs qui soumettent des merge requests pour ajouter des fonctionnalités, corriger des bugs et améliorer le projet.
- **Testeurs**: Les testeurs aident à garantir la qualité du projet en signalant les bugs et en testant les nouvelles fonctionnalités.

### Licence

Ce projet est publié sous la licence [MIT](./LICENSE.md). Voir le fichier LICENSE pour plus de détails.

### Respecter les autres contributeurs

Nous attendons de tous les contributeurs qu'ils fassent preuve de respect et de courtoisie envers les autres. Les commentaires et les discussions doivent être constructifs et professionnels.

Nous vous remercions de votre intérêt pour ce projet et de votre contribution !

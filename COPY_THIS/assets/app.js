/*
* Welcome to your app's main JavaScript file!
*
* This file will be included onto the page via the importmap() Twig function,
* which should already be in your base.html.twig.
*/

// Stimulus
import './bootstrap.js';

// Bootstrap
// TODO - (un)comment if you want to use Bootstrap 
// import './vendor/bootstrap/bootstrap.index.js';
// import './vendor/bootstrap/dist/css/bootstrap.min.css';

// Bootstrap icons
// TODO - (un)comment if you want to use Bootstrap icons
// import './vendor/bootstrap-icons/font/bootstrap-icons.min.css';

// jQuery
// TODO - (un)comment if you want to use jQuery
// import './vendor/jquery/jquery.index.js';

////////
// JS //
////////
// * Import your JS files here
import './js/main.js';

/////////
// CSS //
/////////
// * Import your CSS file here
import './styles/app.css';

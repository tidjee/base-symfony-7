# - VARIABLES ------------- #
SHELL := /bin/bash
FOLDER := $(shell basename $(CURDIR))

# ---- COLORS ---- #
ifneq (,$(findstring xterm,${TERM}))
	BLACK     		:= $(shell tput -Txterm setaf 0)
	RED        		:= $(shell tput -Txterm setaf 1)
	GREEN      	  	:= $(shell tput -Txterm setaf 2)
	YELLOW      	:= $(shell tput -Txterm setaf 3)
	BLUE  			:= $(shell tput -Txterm setaf 4)
	PURPLE      	:= $(shell tput -Txterm setaf 5)
	CYAN         	:= $(shell tput -Txterm setaf 6)
	WHITE        	:= $(shell tput -Txterm setaf 7)
	BLACK_BG        := $(shell tput -Txterm setab 0)
	RED_BG          := $(shell tput -Txterm setab 1)
	GREEN_BG        := $(shell tput -Txterm setab 2)
	YELLOW_BG       := $(shell tput -Txterm setab 3)
	BLUE_BG  		:= $(shell tput -Txterm setab 4)
	PURPLE_BG       := $(shell tput -Txterm setab 5)
	CYAN_BG         := $(shell tput -Txterm setab 6)
	WHITE_BG        := $(shell tput -Txterm setab 7)

	BOLD 		 := $(shell tput -Txterm bold)
	ITALIC       := $(shell tput -Txterm sitm)
	UNDERLINE    := $(shell tput -Txterm smul)

	TITLE        := $(shell tput -Txterm setaf 7 && tput -Txterm bold && tput -Txterm smul)
	SUBTITLE     := $(shell tput -Txterm setaf 4 && tput -Txterm bold && tput -Txterm setab 7)
	INFO         := $(shell tput -Txterm setaf 6)
	SUCCESS      := $(shell tput -Txterm setaf 2 && tput -Txterm bold)
	WARNING      := $(shell tput -Txterm setaf 3)
	ERROR        := $(shell tput -Txterm setaf 1 && tput -Txterm bold && tput -Txterm smul)
	RESET 		 := $(shell tput -Txterm sgr0)
else
	BLACK        := ""
	RED          := ""
	GREEN        := ""
	YELLOW       := ""
	BLUE  		 := ""
	PURPLE       := ""
	CYAN         := ""
	WHITE        := ""
	BLACK_BG     := ""
	RED_BG       := ""
	GREEN_BG     := ""
	YELLOW_BG    := ""
	BLUE_BG      := ""
	PURPLE_BG    := ""
	CYAN_BG      := ""
	WHITE_BG     := ""
	BOLD         := ""
	ITALIC       := ""
	TITLE        := ""
	SUBTITLE     := ""
	INFO         := ""
	SUCCESS      := ""
	WARNING      := ""
	ERROR        := ""
	RESET        := ""
endif

success = ✅ ${SUCCESS}
warning = ⚠️  ${WARNING}
error =${YELLOW_BG} ❌ ${ERROR}
# ----------------- #

# ---- GIT ---------- #
current_date= $(shell date "+%Y-%m-%d_%H:%M:%S")

current_user= $(shell whoami)

commit_message="${current_user}__${current_date}"
# ----------------- #

# ---- DOCKER ---- #
DOCKER := docker
DOCKER_RUN = $(DOCKER) run
DOCKER_EXEC = $(DOCKER) exec -it
DOCKER_COMPOSE = $(DOCKER) compose
DOCKER_COMPOSE_UP = $(DOCKER_COMPOSE) up -d
DOCKER_COMPOSE_LOGS = $(DOCKER_COMPOSE) logs
DOCKER_COMPOSE_STOP = $(DOCKER_COMPOSE) stop
DOCKER_COMPOSE_DOWN = $(DOCKER_COMPOSE) down
DOCKER_CONTAINERS = $(DOCKER) ps -a --filter status=running --format '{{.Names}}'
CONTAINERS_LIST := $(shell $(DOCKER_CONTAINERS) | cut -d ' ' -f 1)
# ----------------- #

# ---- SYMFONY ---- #
SYMFONY = symfony
SYMFONY_SERVER_START = $(SYMFONY) serve -d
SYMFONY_SERVER_STOP = $(SYMFONY) server:stop
SYMFONY_CONSOLE = $(SYMFONY) console
SYMFONY_LINT = $(SYMFONY_CONSOLE) lint:
# ----------------- #

# ---- COMPOSER ---- #
COMPOSER = composer
COMPOSER_REQUIRE = $(COMPOSER) require
COMPOSER_INSTALL = $(COMPOSER) install
COMPOSER_UPDATE = $(COMPOSER) update
RECIPES = $(COMPOSER) recipes
RECIPES_LIST = $(shell $(RECIPES) | cut -d ' ' -f 1)
# ----------------- #

# ---- ASSETMAPPER ---- #
ASSETMAPPER = assetmapper
ASSETMAP = asset-map
IMPORTMAP = importmap
IMPORTMAP_REQUIRE = IMPORTMAP:require
# ----------------- #

# ---- PHPQA ---- #
PHPQA = jakzal/phpqa
PHPQA_RUN = $(DOCKER_RUN) --init -it --rm -v $(PWD):/project -w /project $(PHPQA)
# ----------------- #

# ---- PHPUNIT ---- #
PHPUNIT = APP_ENV=test $(SYMFONY) php bin/phpunit
# ----------------- #

## === 🆘  HELP ================================================================
help: ## Display this help
	@echo "${TITLE}Symfony-And-Docker-Makefile${RESET}"
	@echo "---------------------------"
	@echo ""
	@echo "${SUBTITLE}Targets:${RESET}"
	@#awk '/^##/ {gsub("## ", ""); printf "${GREEN}%-30s${RESET} %s\n", $$1, $$2}; /^[%a-zA-Z_-]+:/ {printf "${GREEN}%-30s${RESET} %s\n", $$1, $$NF}' $(MAKEFILE_LIST)
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "${GREEN}%-30s${RESET} %s\n", $$1, $$2}' | sed -e "s/##/\n${BLACK_BG}${WHITE}${BOLD}/"
	@echo ""
	@echo "${INFO}${UNDERLINE}${BOLD}${}To use a specific target, run:${RESET} make [target]"
.PHONY: help
#---------------------------------------------#

## === 🎨 COLORS & STYLING =====================================================
color: ## Show fonts and background colors in a pretty way (double-entry table format)
	@echo ""
	@echo "${SUBTITLE}LIST OF COLORS${RESET}"
	@echo ""
	@echo "  Font\BG  | ${BLACK_BG}  BLACK  ${RESET} | ${RED_BG}  RED   ${RESET} | ${GREEN_BG} GREEN  ${RESET} | ${YELLOW_BG}${BLACK} YELLOW ${RESET} | ${BLUE_BG}  BLUE  ${RESET} | ${PURPLE_BG} PURPLE ${RESET} | ${CYAN_BG}  CYAN  ${RESET} | ${WHITE_BG}${BLACK} WHITE  ${RESET} |"
	@echo "  ---------|-----------|----------|----------|----------|----------|----------|----------|----------|"
	@echo " *  ${BLACK}BLACK${RESET}  | ${BLACK_BG}${BLACK} Aa-19*  ${RESET} | ${RED_BG}${BLACK} Aa-19* ${RESET} | ${GREEN_BG}${BLACK} Aa-19* ${RESET} | ${YELLOW_BG}${BLACK} Aa-19* ${RESET} | ${BLUE_BG}${BLACK} Aa-19* ${RESET} | ${PURPLE_BG}${BLACK} Aa-19* ${RESET} | ${CYAN_BG}${BLACK} Aa-19* ${RESET} | ${WHITE_BG}${BLACK} Aa-19* ${RESET} |"
	@echo " *  ${RED}RED${RESET}    | ${BLACK_BG}${RED} Aa-19*  ${RESET} | ${RED_BG}${RED} Aa-19* ${RESET} | ${GREEN_BG}${RED} Aa-19* ${RESET} | ${YELLOW_BG}${RED} Aa-19* ${RESET} | ${BLUE_BG}${RED} Aa-19* ${RESET} | ${PURPLE_BG}${RED} Aa-19* ${RESET} | ${CYAN_BG}${RED} Aa-19* ${RESET} | ${WHITE_BG}${RED} Aa-19* ${RESET} |"
	@echo " *  ${GREEN}GREEN${RESET}  | ${BLACK_BG}${GREEN} Aa-19*  ${RESET} | ${RED_BG}${GREEN} Aa-19* ${RESET} | ${GREEN_BG}${GREEN} Aa-19* ${RESET} | ${YELLOW_BG}${GREEN} Aa-19* ${RESET} | ${BLUE_BG}${GREEN} Aa-19* ${RESET} | ${PURPLE_BG}${GREEN} Aa-19* ${RESET} | ${CYAN_BG}${GREEN} Aa-19* ${RESET} | ${WHITE_BG}${GREEN} Aa-19* ${RESET} |"
	@echo " *  ${YELLOW}YELLOW${RESET} | ${BLACK_BG}${YELLOW} Aa-19*  ${RESET} | ${RED_BG}${YELLOW} Aa-19* ${RESET} | ${GREEN_BG}${YELLOW} Aa-19* ${RESET} | ${YELLOW_BG}${YELLOW} Aa-19* ${RESET} | ${BLUE_BG}${YELLOW} Aa-19* ${RESET} | ${PURPLE_BG}${YELLOW} Aa-19* ${RESET} | ${CYAN_BG}${YELLOW} Aa-19* ${RESET} | ${WHITE_BG}${YELLOW} Aa-19* ${RESET} |"
	@echo " *  ${BLUE}BLUE${RESET}   | ${BLACK_BG}${BLUE} Aa-19*  ${RESET} | ${RED_BG}${BLUE} Aa-19* ${RESET} | ${GREEN_BG}${BLUE} Aa-19* ${RESET} | ${YELLOW_BG}${BLUE} Aa-19* ${RESET} | ${BLUE_BG}${BLUE} Aa-19* ${RESET} | ${PURPLE_BG}${BLUE} Aa-19* ${RESET} | ${CYAN_BG}${BLUE} Aa-19* ${RESET} | ${WHITE_BG}${BLUE} Aa-19* ${RESET} |"
	@echo " *  ${PURPLE}PURPLE${RESET} | ${BLACK_BG}${PURPLE} Aa-19*  ${RESET} | ${RED_BG}${PURPLE} Aa-19* ${RESET} | ${GREEN_BG}${PURPLE} Aa-19* ${RESET} | ${YELLOW_BG}${PURPLE} Aa-19* ${RESET} | ${BLUE_BG}${PURPLE} Aa-19* ${RESET} | ${PURPLE_BG}${PURPLE} Aa-19* ${RESET} | ${CYAN_BG}${PURPLE} Aa-19* ${RESET} | ${WHITE_BG}${PURPLE} Aa-19* ${RESET} |"
	@echo " *  ${CYAN}CYAN${RESET}   | ${BLACK_BG}${CYAN} Aa-19*  ${RESET} | ${RED_BG}${CYAN} Aa-19* ${RESET} | ${GREEN_BG}${CYAN} Aa-19* ${RESET} | ${YELLOW_BG}${CYAN} Aa-19* ${RESET} | ${BLUE_BG}${CYAN} Aa-19* ${RESET} | ${PURPLE_BG}${CYAN} Aa-19* ${RESET} | ${CYAN_BG}${CYAN} Aa-19* ${RESET} | ${WHITE_BG}${CYAN} Aa-19* ${RESET} |"
	@echo " *  ${WHITE}WHITE${RESET}  | ${BLACK_BG}${WHITE} Aa-19*  ${RESET} | ${RED_BG}${WHITE} Aa-19* ${RESET} | ${GREEN_BG}${WHITE} Aa-19* ${RESET} | ${YELLOW_BG}${WHITE} Aa-19* ${RESET} | ${BLUE_BG}${WHITE} Aa-19* ${RESET} | ${PURPLE_BG}${WHITE} Aa-19* ${RESET} | ${CYAN_BG}${WHITE} Aa-19* ${RESET} | ${WHITE_BG}${WHITE} Aa-19* ${RESET} |"
	@echo ""
.PHONY: color

format: ## Show all the formats
	@echo ""
	@echo "${SUBTITLE}LIST OF FORMATS${RESET}"
	@echo ""
	@echo "${BOLD}BOLD | bold | Bold${RESET}"
	@echo "${ITALIC}ITALIC | italic | Italic${RESET}"
	@echo "${UNDERLINE}UNDERLINE | underline | Underline${RESET}"
.PHONY: format

style: ## Show all the styles
	@echo ""
	@echo "${SUBTITLE}LIST OF STYLES${RESET}"
	@echo ""
	@echo "${TITLE}TITLE | title | Title${RESET}"
	@echo "${SUBTITLE}SUBTITLE | subtitle | Subtitle${RESET}"
	@echo "${INFO}INFO | info | Info${RESET}"
	@echo "${SUCCESS}SUCCESS | success | Success${RESET}"
	@echo "${success}All good!${RESET}"
	@echo "${WARNING}WARNING | warning | Warning${RESET}"
	@echo "${warning}There is a warning!${RESET}"
	@echo "${ERROR}ERROR | error | Error${RESET}"
	@echo "${error}ERROR: There is an error!${RESET}"

.PHONY: style
#---------------------------------------------#

## === 🐙  GIT =================================================================
git-credentials: ## Git credentials
	@echo ""
	@git config --global credential.helper 'store --file ~/.my-credentials'
	@echo ""
	@echo "${success}.my-credentials file created in ~ folder!${RESET}"
	@echo "${INFO}> The next time you enter your credentials, they will be stored in ~/.my-credentials!${RESET}"
.PHONY: git-credentials
#---------------------------------------------#

## === 🐋  DOCKER ==============================================================
docker-start: ## Start Docker containers
	@$(DOCKER_COMPOSE_UP) -d
	@echo "${success}Docker containers started!${RESET}"
.PHONY: docker-start

docker-logs: ## Show Docker containers logs
	@$(DOCKER_COMPOSE_LOGS)
.PHONY: docker-logs

docker-stop: ## Stop Docker containers
	@$(DOCKER_COMPOSE_STOP)
	@echo "${success}Docker containers stopped!${RESET}"
.PHONY: docker-stop

docker-restart: ## Restart Docker containers
	@$(MAKE) docker-stop docker-start
	@echo "${success}Docker containers restarted!${RESET}"
.PHONY: docker-restart

docker-down: ## Stop and remove Docker containers
	@$(DOCKER_COMPOSE_DOWN -v)
	@echo "${success}Docker containers removed!${RESET}"
.PHONY: docker-down

docker-bash: ## Enter into Docker container in bash
	@echo ""
	@echo "${TITLE}List of available containers:${RESET}"
	@echo ""
	@echo "${SUBTITLE}Select a container to enter:${RESET}"
	@select container in ${CONTAINERS_LIST}; do \
		if [ -n "$$container" ]; then \
			clear && \
			echo ""; \
			echo "${INFO}Entering into container ...${RESET}"; \
			echo ""; \
			echo "Container name: ${YELLOW}${BOLD} $$container ${RESET}"; \
			echo ""; \
			echo "${INFO}INFO:${RESET} To exit from the container, type ${GREEN}exit${RESET} and press ${GREEN}Enter${RESET} key."; \
			echo ""; \
			echo "-----------------------------"; \
			echo ""; \
			$(DOCKER_EXEC) $$container bash; \
			echo ""; \
			break; \
		else \
			echo ""; \
			echo "${error}Invalid selection! Please select a valid container.${RESET}"; \
			echo ""; \
			echo "${INFO}INFO:${RESET} To list available containers, type ${GREEN}make docker-bash${RESET}"; \
			echo ""; \
			echo "${INFO}INFO:${RESET} To quit the script, press ${GREEN}Ctrl+C${RESET}"; \
		fi; \
	done
.PHONY: docker-bash
#---------------------------------------------#

## === 🎛️  SYMFONY ============================================================
sf-start: ## Start Symfony server
	@echo "${TITLE}Start Symfony server${RESET}"
	@echo ""
	@$(SYMFONY_SERVER_START)
	@echo "${success}Symfony server started!${RESET}"
.PHONY: sf-start

sf-stop: ## Stop Symfony server
	@echo "${TITLE}Stop Symfony server${RESET}"
	@echo ""
	@$(SYMFONY_SERVER_STOP)
	@echo "${success}Symfony server stopped!${RESET}"
.PHONY: sf-stop

sf-restart: ## Restart Symfony server
	@$(MAKE) sf-stop sf-start
	@echo "${success}Symfony server restarted!${RESET}"
.PHONY: sf-restart

sf-cc: ## Clear Symfony cache
	@echo "${TITLE}Clear Symfony cache${RESET}"
	@echo ""
	@$(SYMFONY_CONSOLE) cache:clear
	@$(SYMFONY_CONSOLE) cache:warmup
	@echo "${success}Cache cleared!${RESET}";
.PHONY: sf-cc

sf-log: ## Show Symfony server logs
	@echo "${TITLE}Show Symfony logs${RESET}"
	@echo ""
	@echo "${INFO}INFO:${RESET} To exit from the Symfony server logs, type ${GREEN}Ctrl + C$${RESET} keys.";
	@echo ""
	@echo "${SUBTITLE}There are the Symfony server logs:${RESET}"
	@echo ""
	@$(SYMFONY) server:log
.PHONY: sf-log

sf-sudo-perm: ## Fix permissions with sudo
	@sudo chmod -R 777 var
.PHONY: sf-sudo-perm

sf-dump-env: ## Dump env
	@$(SYMFONY_CONSOLE) debug:dotenv
.PHONY: sf-dump-env

sf-dump-env-container: ## Dump Env container
	@$(SYMFONY_CONSOLE) debug:container --env-vars
.PHONY: sf-dump-env-container

sf-dump-routes: ## Dump routes
	@$(SYMFONY_CONSOLE) debug:router
.PHONY: sf-dump-routes

sf-check-requirements: ## Check requirements
	@$(SYMFONY) check:requirements
.PHONY: sf-check-requirements
#---------------------------------------------#

## === 🐘  DATABASE ============================================================
db-create: ## Create the database
	@echo "${TITLE}Create the database${RESET}"
	@echo ""
	@$(SYMFONY_CONSOLE) doctrine:database:create --if-not-exists
	@echo "${success}Database created!${RESET}";
.PHONY: db-create

db-drop: ## Drop the database
	@echo "${TITLE}Drop the database${RESET}"
	@echo ""
	@$(SYMFONY_CONSOLE) doctrine:database:drop --force --if-exists
	@echo "${success}Database dropped!${RESET}";
.PHONY: db-drop

db-migrations: ## Make migrations
	@echo "${TITLE}Make migrations${RESET}"
	@echo ""
	@$(SYMFONY_CONSOLE) make:migration --no-interaction
	@echo "${success}Migrations created!${RESET}";
.PHONY: db-migrations

db-migrate: ## Migrate the database
	@echo "${TITLE}Migrate the database${RESET}"
	@echo ""
	@$(SYMFONY_CONSOLE) doctrine:migrations:migrate --no-interaction
	@echo "${success}Database migrated!${RESET}";
.PHONY: db-migrate

db-init: ## Initialize the database
	@echo "${TITLE}Initialize the database${RESET}"
	@echo ""
	@$(MAKE) db-drop db-create db-migrations db-migrate
	@echo ""
	@echo "${success}Database initialized!${RESET}"
.PHONY: db-init

db-reset: ## Reset database.
	@echo "${INFO}Reseting database ...${RESET}"
	@echo ""
	@$(MAKE) db-drop db-create db-migrate
	@echo ""
	@echo "${success}Database reset!${RESET}"
.PHONY: db-reset
#---------------------------------------------#

## === 🐘  FIXTURES ============================================================
fixt-bundle: ## Add fixtures and fakerphp/faker bundles
	@$(COMPOSER_REQUIRE) --dev doctrine/doctrine-fixtures-bundle
	@echo ""
	@echo "${success}Fixtures bundle added for dev environment!${RESET}"
	@echo ""
	@$(COMPOSER_REQUIRE) --dev fakerphp/faker
	@echo ""
	@echo "${success}FakerPHP fixtures bundle added for dev environment!${RESET}"
.PHONY: fixt-bundle

fixt-foundry: ## Add Foundry fixtures bundle
	@$(COMPOSER_REQUIRE) --dev zenstruck/foundry
	@echo ""
	@echo "${success}Foundry fixtures bundle added for dev environment!${RESET}"
	@echo ""
	# Ask if you want to add create factory for all fields of your entities
	@read -p "Do you want to create factory for all fields of your entities ? (y/n) " ANSWER
	@if [ "$ANSWER" = "y|Y|yes|YES" ]; then \
		${SYMFONY_CONSOLE} make:factory --all-fields; \
		echo ""; \
		echo "${success}Factories created for all fields of selected entities!${RESET}"; \
	else [ "$ANSWER" = "n|N|no|NO" ] \
		${SYMFONY_CONSOLE} make:factory; \
		echo ""; \
		echo "${success}Factories created for selected entities!${RESET}"; \
.PHONY: fixt-foundry

fixt-load: ## Load the fixtures
	@echo "${TITLE}Load the fixtures${RESET}"
	@echo ""
	@$(SYMFONY_CONSOLE) doctrine:fixtures:load --no-interaction
	@echo ""
	@echo "${success}Fixtures loaded!${RESET}"
.PHONY: fixt-load
#---------------------------------------------#

## === 🐘  MAKER BUNDLE ========================================================
sf-entity: ## Make Symfony entity
	@$(SYMFONY_CONSOLE) make:entity
	@echo ""
	@echo "${success}Entity created!${RESET}"
.PHONY: sf-entity

sf-user: ## Make Symfony user entity
	@$(SYMFONY_CONSOLE) make:user
	@echo ""
	@echo "${success}User entity created!${RESET}"
.PHONY: sf-user

sf-auth: ## Make Symfony authenticator
	@$(SYMFONY_CONSOLE) make:auth
	@echo ""
	@echo "${success}Authenticator created!${RESET}"
.PHONY: sf-auth

sf-controller: ## Make Symfony controller
	@$(SYMFONY_CONSOLE) make:controller
	@echo ""
	@echo "${success}Controller created!${RESET}"
.PHONY: sf-controller

sf-form: ## Make Symfony form
	@$(SYMFONY_CONSOLE) make:form
	@echo ""
	@echo "${success}Form created!${RESET}"
.PHONY: sf-form

sf-reg-form: ## Make Symfony registration form
	@$(SYMFONY_CONSOLE) make:registration-form
	@echo ""
	@echo "${success}Registration form created!${RESET}"
.PHONY: sf-reg-form

sf-subscriber: ## Make Symfony event subscriber
	@$(SYMFONY_CONSOLE) make:subscriber
	@echo ""
	@echo "${success}Event subscriber created!${RESET}"
.PHONY: sf-subscriber

sf-crud: ## Make Symfony crud
	@$(SYMFONY_CONSOLE) make:crud
	@echo ""
	@echo "${success}Crud created!${RESET}"
.PHONY: sf-crud
#---------------------------------------------#

## === 🐘 EASYADMIN ============================================================
sf-ea: ## Add  EasyAdminBundle
	@$(COMPOSER_REQUIRE) easycorp/easyadmin-bundle
	@echo ""
	@echo "${success}EasyAdminBundle added!${RESET}"
	@echo ""
	@$(SYMFONY_CONSOLE) make:admin:dashboard
	@echo ""
	@echo "${success}Admin dashboard created!${RESET}"
	@echo ""
	@$(MAKE) sf-ea-crud
.PHONY: sf-ea

sf-ea-crud: ## Make Symfony easyadmin crud
	@$(SYMFONY_CONSOLE) make:admin:crud
	@echo ""
	@echo "${success}EasyAdmin crud created!${RESET}"
.PHONY: sf-ea-crud
#---------------------------------------------#

## === 📦  COMPOSER ============================================================
composer-install: ## Install composer dependencies
	@$(COMPOSER_INSTALL)
	@echo ""
	@echo "${success}Composer dependencies installed!${RESET}"
.PHONY: composer-install

composer-update: ## Update composer dependencies
	@$(COMPOSER_UPDATE)
	@echo ""
	@echo "${success}Composer dependencies updated!${RESET}"
.PHONY: composer-update

composer-validate: ## Validate composer.json file
	@$(COMPOSER) validate
.PHONY: composer-validate

composer-validate-deep: ## Validate composer.json and composer.lock files in strict mode
	@$(COMPOSER) validate --strict --check-lock
.PHONY: composer-validate-deep

composer-remove: ## Remove composer recipe
	@echo "${TITLE}Remove composer recipe${RESET}"
	@echo ""
	@echo "Select a recipe to remove: "
	@select recipe in $(RECIPES_LIST); do \
	if [ -n "$$recipe" ]; then \
		echo "${INFO}Recipe selected: $$recipe${RESET}"; \
		echo ""; \
		echo "${INFO}Removing recipe...${RESET}"; \
		echo ""; \
		$(COMPOSER_REMOVE) $$recipe; \
		echo "${success}Recipe removed!${RESET}"; \
	else \
		echo "${ERROR}Invalid recipe!${RESET}"; \
		echo ""; \
	fi; \
	done

.PHONY: composer-remove
#---------------------------------------------#

## === 🌐  ASSETMAPPER =========================================================
am-install: ## Install assetMapper
	@$(COMPOSER_REQUIRE) symfony/asset-mapper symfony/asset symfony/twig-pack
	@echo ""
	@echo "${success}AssetMapper installed!${RESET}"
.PHONY: am-install

am-bootstrap: ## Add Bootstrap with AssetMapper
	@$(SYMFONY_CONSOLE) $(IMPORTMAP_REQUIRE) bootstrap
	@echo ""
	@echo "${success}Bootstrap added!${RESET}"
.PHONY: am-bootstrap

am-bootstrap-icons: ## Add Bootstrap Icons with AssetMapper
	@$(SYMFONY_CONSOLE) $(IMPORTMAP_REQUIRE) bootstrap-icons/font/bootstrap-icons.min.css
	@echo ""
	@echo "${success}Bootstrap Icons added!${RESET}"
.PHONY: am-bootstrap-icons

am-jquery: ## Add jQuery with AssetMapper
	@$(SYMFONY_CONSOLE) $(IMPORTMAP_REQUIRE) jquery
	@echo ""
	@echo "${success}jQuery added!${RESET}"
.PHONY: am-jquery

am-htmx: ## Add HTMX with AssetMapper
	@$(SYMFONY_CONSOLE) $(IMPORTMAP_REQUIRE) htmx.org
	@echo ""
	@echo "${success}HTMX added!${RESET}"
.PHONY: am-htmx

am-tailwind: ## Add tailwind with AssetMapper
	@$(COMPOSER_REQUIRE) symfonycasts/tailwind-bundle
	@echo ""
	@$(SYMFONY_CONSOLE) tailwind:init
	@echo "${success}Tailwind added!${RESET}"
.PHONY: am-tailwind

am-tailwind-watch: ## Build and Minify tailwind file --watch
	@$(SYMFONY_CONSOLE) tailwind:build --minify --watch -v
.PHONY: npm-tailwind-watch

am-compile: ## Build and Minify tailwind file & Compile assets
	@$(SYMFONY_CONSOLE) tailwind:build --minify
	@$(SYMFONY_CONSOLE) $(ASSETMAP):compile
.PHONY: npm-compile

am-tailwind-config: ## Show the full config of tailwind
	@$(SYMFONY_CONSOLE) config:dump symfonycasts_tailwind
.PHONY: am-tailwind-config

am-debug: ## Show all the mapped paths and assets inside of each
	@$(SYMFONY_CONSOLE) debug:$(ASSETMAP) --full
.PHONY: am-debug
#---------------------------------------------#

## === 🐛 TESTS ================================================================

## === 🐛 DATABASE ENV=TEST ====================================================
test-db-init: ## Create database for test environment
	@$(SYMFONY_CONSOLE) doctrine:database:drop --if-exists --force --env=test --no-interaction && \
	$(SYMFONY_CONSOLE) doctrine:database:create --if-not-exists --env=test --no-interaction && \
	$(SYMFONY_CONSOLE) doctrine:migrations:migrate --env=test --no-interaction && \
	echo "" && \
	echo "${success}Env=test database created!${RESET}"
.PHONY: test-db-init

test-db-load-fixt: ## Load fixtures for test environment
	@$(SYMFONY_CONSOLE) doctrine:fixtures:load --env=test --no-interaction
	@echo ""
	@echo "${success}Env=test fixtures loaded!${RESET}"
.PHONY: test-db-load-fixt
#---------------------------------------------#

## === 🐛 TESTS SETUP ==========================================================
test-make: ## Create tests
	@$(SYMFONY_CONSOLE) make:test
.PHONY: test-make

test-run: ## Run tests
	@$(PHPUNIT) --testdox
.PHONY: tests

test-coverage: ## Run tests with coverage (Run in Docker Container ONLY)
	@./vendor/bin/phpunit --coverage-html ./tests/tests-coverage
	@echo ""
	@echo "${success}Tests coverage created!${RESET}"
	@echo ""
	@echo "${INFO}Open tests coverage in browser:${RESET} http://localhost:5500/"${FOLDER}"/tests/tests-coverage/index.html"
.PHONY: tests-coverage
#---------------------------------------------#

## === 🐛 RECTOR ===============================================================
rector-install: ## Install rector
	@$(COMPOSER_REQUIRE)  --dev rector/rector
	@echo ""
	@echo "${success}Rector installed!${RESET}"
.PHONY: rector-install

rector-run-dry: ## Run rector
	@vendor/bin/rector process src --dry-run
.PHONY: rector-run-dry

rector-run: ## Run rector
	@vendor/bin/rector process src
.PHONY: rector-run
#---------------------------------------------#

## === 🐛 PHPQA (Run these outside of Docker Container ONLY) ===================
qa-cs-fixer-dry-run: ## Run php-cs-fixer in dry-run mode
	@$(PHPQA_RUN) php-cs-fixer fix ./src --rules=@Symfony --verbose --dry-run
	@echo ""
	@echo "${success}PHP CS Fixer tests runned in dry-run successfully!${RESET}"
.PHONY: qa-cs-fixer-dry-run

qa-cs-fixer: ## Run php-cs-fixer
	@$(PHPQA_RUN) php-cs-fixer fix ./src --rules=@Symfony --verbose
	@echo ""
	@echo "${success}PHP CS Fixer tests runned successfully!${RESET}"
	@echo ""
.PHONY: qa-cs-fixer

qa-phpstan: ## Run phpstan
	@$(PHPQA_RUN) phpstan analyse ./src --level=7
	@echo ""
	@echo "${success}PHPStan tests runned successfully!${RESET}"
	@echo ""
.PHONY: qa-phpstan

qa-security-checker: ## Run security-checker
	@$(SYMFONY) security:check
	@echo ""
	@echo "${success}Security checker tests runned successfully!${RESET}"
	@echo ""
.PHONY: qa-security-checker

qa-phpcpd: ## Run phpcpd (copy/paste detector)
	@$(PHPQA_RUN) phpcpd ./src
	@echo ""
	@echo "${success}phpcpd tests runned successfully!${RESET}"
.PHONY: qa-phpcpd

qa-php-metrics: ## Run php-metrics
	@$(PHPQA_RUN) phpmetrics --report-html=tests/phpmetrics ./src
	@echo ""
	@echo "${success}php-metrics tests runned successfully!${RESET}"
	@echo ""
	@echo "${INFO}Open php-metrics in browser:${RESET} http://localhost:5500/"$(FOLDER)"/tests/phpmetrics/index.html"
.PHONY: qa-php-metrics

before-commit: ## Run before-commit
	@$(MAKE) qa-cs-fixer qa-phpstan qa-security-checker qa-phpcpd qa-php-metrics
	@echo ""
	@echo "${success}All tests passed successfully!${RESET}"
.PHONY: before-commit
#---------------------------------------------#

## === 🐛 SYMFONY LINT (Run in Docker Container ONLY) =========================================================
qa-lint-twigs: ## Lint twig files
	@$(SYMFONY_LINT)twig ./templates
	@echo ""
	@echo "${success}Twig linted!${RESET}"
.PHONY: qa-lint-twigs

qa-lint-yaml: ## Lint yaml files
	@$(SYMFONY_LINT)yaml ./config
	@echo ""
	@echo "${success}Yaml linted!${RESET}"
.PHONY: qa-lint-yaml

qa-lint-container: ## Lint container
	@$(SYMFONY_LINT)container
	@echo ""
	@echo "${success}Container linted!${RESET}"
	@echo ""
.PHONY: qa-lint-container

qa-audit: ## Run composer audit
	@$(COMPOSER) audit
	@echo ""
	@echo "${success}Composer audit done!${RESET}"
	@echo ""
.PHONY: qa-audit

qa-lint-all: ## Lint all
	@$(MAKE) qa-lint-twigs qa-lint-yaml qa-lint-container test-coverage qa-audit rector-run
	@echo ""
	@echo "${success}All linted!${RESET}"
.PHONY: qa-lint-all
#---------------------------------------------#

## === 🐛 DOCTRINE (Run in Docker Container ONLY) ==============================
qa-lint-schema: ## Lint Doctrine schema
	@$(SYMFONY_CONSOLE) doctrine:schema:validate --skip-sync -vvv --no-interaction
	@echo ""
	@echo "${success}Schema linted!${RESET}"
.PHONY: qa-lint-schema

qa-lint-db: ## Lint database
	@$(SYMFONY_CONSOLE) doctrine:schema:validate -vvv --no-interaction
	@echo ""
	@echo "${success}Database linted!${RESET}"
.PHONY: qa-lint-db
#---------------------------------------------#
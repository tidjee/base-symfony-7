# Base Symfony 7

[English version here](./docs/README-en.md)

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=false} -->

## Table des matières

<!-- code_chunk_output -->

- [Table des matières](#table-des-matières)
- [Description](#description)
  - [Fichiers](#fichiers)
  - [Pré-requis](#pré-requis)
    - [Installation des pré-requis](#installation-des-pré-requis)
      - [PHP](#php)
      - [Docker](#docker)
      - [Portainer](#portainer)
      - [Composer](#composer)
      - [CLI Symfony](#cli-symfony)
      - [Git](#git)
- [Installation](#installation)
- [Containers Docker](#containers-docker)
- [Explication des fichiers dans le dossier `COPY_THIS`](#explication-des-fichiers-dans-le-dossier-copy_this)
  - [Dossier `docker`](#dossier-docker)
    - [Dossier `docker/apache`](#dossier-dockerapache)
    - [Dossier `docker/php83`](#dossier-dockerphp83)
    - [Dossier `docker/phpma`](#dossier-dockerphpma)
  - [Dossier `lazygit`](#dossier-lazygit)
  - [Fichier `.env.NEW`](#fichier-envnew)
  - [Fichier `.gitignore.NEW`](#fichier-gitignorenew)
  - [Fichier `compose.yaml.NEW`](#fichier-composeyamlnew)
  - [Fichier `Makefile`](#fichier-makefile)
  - [Fichier `rector.php`](#fichier-rectorphp)
- [Contributions](#contributions)
- [Licence](#licence)

<!-- /code_chunk_output -->

## Description

Ce dépôt fournit un ensemble de fichiers de configuration de base pour vous aider à démarrer rapidement votre projet Symfony 7. Il inclut:

- Un dossier `docker` contenant les fichiers pour lancer votre projet en conteneurs avec Docker.
- Un fichier `.env.NEW` contenant des exemples de variables d'environnement.
- Un fichier `.gitignore.NEW` pour configurer les chemins ignorés par Git.
- Un fichier `compose.yaml.NEW` pour configurer et exécuter les conteneurs Docker.
- Un fichier `Makefile` pour simplifier les tâches courantes.

> ATTENTION: L'image PHP de ce dépot **NE CONTIENT NI nodejs, NI npm** car Symfony 7 recommande d'utiliser [Asset Mapper](https://symfony.com/doc/current/frontend.html#frontend-asset-mapper).

### Fichiers

Ce dépot comprend:

```md
├── COPY_THIS
│   ├── docker
│   │   ├── apache
│   │   │   └── default.conf
│   │   ├── php83
│   │   │   └── Dockerfile
│   │   └── phpma
│   │   └── Dockerfile
│   ├── .env.NEW
│   ├── .gitignore.NEW
│   ├── compose.yaml.NEW
│   ├── Makefile
│   └── rector.php
├── docs
│   ├── CONTRIBUTING-en.md
│   └── README-en.md
├── lazygit
│   └── lazygit.sh
├── CONTRIBUTING.md
├── LICENSE.md
└── README.md
```

### Pré-requis

- [PHP >= 8.2](https://www.dedicatedcore.com/blog/upgrade-php-ubuntu/) (PHP 8.3 est inclus dans le dépot)
- [Docker](https://www.cherryservers.com/blog/install-portainer-ubuntu)
- [Portainer](https://www.cherryservers.com/blog/install-portainer-ubuntu) ou [Docker Desktop](https://docs.docker.com/get-docker/)
- [Composer](https://getcomposer.org/download/)
- [Symfony CLI](https://symfony.com/download/)
- [Git](https://git-scm.com/downloads)

#### Installation des pré-requis

##### PHP

- Ajouter le dépot `ondrej/php` au système.

```bash
sudo apt install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt update
```

- Installer PHP 8.3 et des extensions.

```bash
sudo apt install php8.3 php8.3-cli php8.3-common php8.3-mbstring php8.3-mysql php8.3-xml php8.3-zip php8.3-curl php8.3-intl
```

- Vérifier que PHP est installe.

```bash
php -v
```

- Si plusieurs versions sont disponibles, il est possible de lancer le script pour lister les version de PHP et choisir celle à utiliser.

```bash
sudo update-alternatives --config php
```

##### Docker

- Installer Docker.

```bash
sudo apt install docker.io -y
```

- Vérifier que Docker est installé.

```bash
sudo systemctl status docker
```

- Si le processus de Docker ne fonctionne pas, il est possible de le redémarrer.

```bash
sudo systemctl restart docker
```

> [Pour utiliser Docker sans `sudo`.](https://julienc.io/blog/utiliser_le_client_docker_sans_etre_root)
>
> ```bash
> sudo groupadd -f docker
> sudo chown root:docker /var/run/docker.sock
> sudo usermod -a -G docker "$(whoami)"
> newgrp docker
> sudo systemctl restart docker
> ```
>
> Maintenant, Docker est utilisable sans `sudo`.

##### Portainer

- Télécharger l'image de Portainer.

```bash
docker pull portainer/portainer-ce:latest
```

- Vérifier que l'image de Portainer est bien téléchargée.

```bash
docker images
```

- Lancer Portainer.

```bash
docker run -d -p 9100:9000 --restart always -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer-ce:latest
```

> Portainer est maintenant accessible sur [http://localhost:9100](http://localhost:9100)

##### Composer

- Installer Composer.

```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

- Vérifier que Composer est installé.

```bash
composer
```

##### CLI Symfony

- Installer la CLI Symfony .

```bash
curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | sudo -E bash
sudo apt install symfony-cli
```

- Vérifier que la CLI Symfony est installé.

```bash
symfony
```

- Vérifier que le système est correctement configuré pour utiliser Symfony.

```bash
symfony check:requirements
```

##### Git

- Installer Git.

```bash
sudo apt install git
```

- Vérifier que Git est installé.

```bash
git --version
```

## Installation

1. Créer un nouveau projet Symfony.

   - Avec la CLI Symfony:

     - Pour une webapp:

     ```bash
     symfony new <NOM_DU_PROJET> -webapp
     ```

     - Pour un microservice:

     ```bash
     symfony new <NOM_DU_PROJET>
     ```

2. Télecharger l'archive du [dépot au complet](https://gitlab.com/tidjee/base-symfony-7/-/archive/main/base-symfony-7-main.zip) ou du [dossier `COPY_THIS`](https://gitlab.com/tidjee/base-symfony-7/-/archive/main/base-symfony-7-main.zip?path=COPY_THIS) uniquement et l'extraire.

3. Copier le contenu du dossier `COPY_THIS` dans le projet nouvellement créé.

4. Adapter le contenu du fichier `.env` avec les variables d'environnement appropriés (`.env.NEW`).

5. Adapter le contenu du fichier `.gitignore` à partir de (`.gitignore.NEW`).

6. Adapter le contenu du fichier `compose.yaml` à partir de (`compose.yaml.NEW`).

7. Copier le fichier `rector.php` à la racine du projet.

8. Vérifier les configurations suivantes:

   - `docker/php83/Dockerfile`
   - `docker/apache/default.conf`
   - `docker/phpma/Dockerfile`

9. Lancer `docker-compose up -d` pour créer et lancer les conteneurs.

10. Lier le projet à un dépot GitLab distant.

    - Pour stocker vos identifiants GitLab localement (**_OPTIONNEL_**):

      ```bash
      make git-credentials
      ```

      > La prochaine fois que vous saissirez vos identifiants GitLab, cette commande va les stocker dans `~/.my-credentials`.

    - Pour ajouter le projet à un dépot GitLab :

      ```bash
      git remote add origin <URL_DU_DEPOT>.git
      ```

      > Ex: `git remote add origin https://gitlab.com/tidjee/base-symfony-7.git`

      ```bash
      git branch -M main
      ```

      ```bash
      git fetch
      ```

      ```bash
      git branch --set-upstream-to=origin/main main
      ```

      ```bash
      git pull --allow-unrelated-histories
      ```

    - Pour commiter les modifications:

      ```bash
      git add .
      git commit -m "📦 NEW: Add Symfony project"
      git push
      ```

11. Installer `lazygit` pour afficher votre dépot Git dans le terminal (GUI).

    ```bash
    bash ~/lazygit.sh
    ```

    Pour ouvrir `lazygit`: tapez la commande `lazygit` dans le terminal à partir d'un dossier où git est initialisé.

    ```bash
    lazygit
    ```

## Containers Docker

- **PHP 8.3-apache**:
  Basé sur l'image [php:8.3-apache](https://hub.docker.com/_/php/), customisé pour Symfony 7.
- **MySQL**
  Basé sur l'image [mysql:latest](https://hub.docker.com/_/mysql/).
- **Adminer**
  Interface d'administration de base de données basé sur l'image [adminer:latest](https://hub.docker.com/_/adminer/).
- **PhpMyAdmin**
  Interface d'administration de base de données basé sur l'image [phpmyadmin/phpmyadmin:latest](https://hub.docker.com/r/phpmyadmin/phpmyadmin/). J'y ai ajouté le theme `BooDark` (theme sombre de Bootstrap).
- **Mailpit**
  Serveur de messagerie (SMTP et HTTP) basé sur l'image [axllent/mailpit:latest](https://hub.docker.com/r/axllent/mailpit/).

## Explication des fichiers dans le dossier `COPY_THIS`

### Dossier `docker`

Le dossier `docker` contient les fichiers de configuration pour lancer votre projet en conteneurs avec Docker.

#### Dossier `docker/apache`

Le dossier `docker/apache` contient le fichier `default.conf`. C'est le fichier de configuration du serveur web Apache par défaut.

```xml
<VirtualHost *:80>
    DocumentRoot /var/www/public
    DirectoryIndex index.php

    ServerName localhost

    <Directory /var/www/public>
        AllowOverride None
        Order Allow,Deny
        Allow from All

        FallbackResource /index.php
    </Directory>

    # uncomment the following lines if you install assets as symlinks
    # or run into problems when compiling LESS/Sass/CoffeeScript assets
    # <Directory /var/www/project>
    #     Options FollowSymlinks
    # </Directory>

    # optionally disable the fallback resource for the asset directories
    # which will allow Apache to return a 404 error when files are
    # not found instead of passing the request to Symfony
    <Directory /var/www/public/bundles>
        FallbackResource disabled
    </Directory>
    ErrorLog /var/log/apache2/project_error.log
    CustomLog /var/log/apache2/project_access.log combined
</VirtualHost>
```

#### Dossier `docker/php83`

Le dossier `docker/php83` contient le fichier `Dockerfile`. C'est le fichier de configuration du conteneur PHP.

```dockerfile
FROM php:8.3-apache

RUN apt-get update \
    &&  apt-get install -y --no-install-recommends \
    locales apt-utils git libicu-dev g++ libpng-dev libxml2-dev libzip-dev libonig-dev libxslt-dev unzip lsd

RUN echo "alias cls='clear'" >> ~/.bashrc \
    echo "alias ls='lsd -al --group-dirs first'" >> ~/.bashrc

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen  \
    &&  echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen \
    &&  locale-gen

RUN curl -sS https://getcomposer.org/installer | php -- \
    &&  mv composer.phar /usr/local/bin/composer

RUN curl -sS https://get.symfony.com/cli/installer | bash \
    &&  mv /root/.symfony5/bin/symfony /usr/local/bin

RUN  docker-php-ext-configure intl

RUN  docker-php-ext-install pdo pdo_mysql opcache intl zip calendar dom mbstring gd xsl \
    &&  pecl install apcu && docker-php-ext-enable apcu

RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

RUN echo "xdebug.coverage_enable" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.mode=coverage" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

RUN  a2enmod rewrite

WORKDIR /var/www

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony5/bin/symfony /usr/local/bin/symfony

RUN git config --global user.email "pinet.donatien@gmail.com"
RUN git config --global user.name "Donatien Pinet"
```

#### Dossier `docker/phpma`

Le dossier `docker/phpma` contient le fichier `Dockerfile`. C'est le fichier de configuration du conteneur PhpMyAdmin.

```dockerfile
FROM phpmyadmin/phpmyadmin:latest

RUN apt-get update && apt-get install -y unzip lsd

RUN echo "alias cls='clear'" >> ~/.bashrc \
    echo "alias ls='lsd -al --group-dirs first'" >> ~/.bashrc

WORKDIR /var/www/html

RUN mkdir -p /var/www/html/themes

# Add of the `BooDark` theme | Ajout du theme `BooDark`
RUN curl -L https://files.phpmyadmin.net/themes/boodark/1.1.0/boodark-1.1.0.zip > /tmp/boodark-1.1.0.zip && \
    unzip /tmp/boodark-1.1.0.zip -d /tmp/boodark-1.1.0/ && rm /tmp/boodark-1.1.0.zip && \
    mv /tmp/boodark-1.1.0/boodark /var/www/html/themes/
```

### Dossier `lazygit`

Le dossier `lazygit` contient le fichier `lazygit.sh`. C'est le script d'installation de lazygit.

```bash
#!/bin/bash

# Colors
info='\033[0;34m'
green='\033[32m'
red='\033[31m'
yellow='\033[33m'
bold='\e[1m'
reset='\033[0m'
success=${green}${bold}✅
error=${red}${bold}❌
warning=${yellow}${bold}⚠

# Install lazygit
cd ~
LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')
curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"
tar xf lazygit.tar.gz lazygit
mv ~/lazygit ~/.lazygit && rm -R lazygit.tar.gz
sudo install ~/.lazygit /usr/local/bin
echo ""
if [ ${?} -ne 0 ]; then
    echo -e "${error} Lazygit not installed${reset}"
    exit 1
else
    echo -e "${success} Lazygit installed${reset}"
    echo ""
    echo -e "Run '${info}lazygit${reset}' to start"
    echo ""
    echo -e "Run '${info}lazygit --help${reset}' for more info"
    echo ""
    echo -e "Run '${info}lazygit --version${reset}' to check version"
    echo ""
fi
```

### Fichier `.env.NEW`

Le fichier `.env.NEW` contient les variables d'environnement appropriés pour votre projet.

```env
##################
###> VARIABLES ###
##################

# These variables are used by compose.yaml | Ces variables sont utilisées par compose.yaml

################################################################################
# !    UPDATE THE VARIABLES IN THIS SECTION BEFORE `docker-compose up -d`      #
#                                ---------------                               #
# ! METS A JOUR LES VARIABLES DANS CETTE SECTION AVANT `docker-compose up -d`  #
################################################################################

# Name of your project | Nom de votre projet
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# TODO CHANGE THE NAME FOR EACH NEW PROJECT | CHANGE LE NOM DE VOTRE PROJET POUR CHAQUE NOUVEAU PROJET
# ? OR DEFAULT NAME `app` | OU NOM PAR DEFAUT `app`
PROJECT_NAME=app

# Database | Base de données
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
MYSQL_PORT=3306          # Port that will be used for MySQL | Port qui sera utilisé pour MySQL
MYSQL_HOST=mysql         # Host that will be used for MySQL | Host qui sera utilisé pour MySQL
MYSQL_ROOT_PASSWORD=toor # Root password | Mot de passe Root
MYSQL_USER=dev           # Development user unsername | Nom de l'utilisateur de developpement
MYSQL_PASSWORD=dev       # Development user password | Mot de passe de l'utilisateur de developpement

# Adminer | Adminer
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
ADMINER_PORT=9001    # Port that will be used for Adminer | Port qui sera utilisé pour Adminer
ADMINER_DESIGN=hydra # Design that will be used for Adminer | Design qui sera utilisé pour Adminer

# Php83-apache | Php83-apache
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
PHP_HTTP_PORT=9000 # Port that will be used for access to Php83-apache web interface | Port qui sera utilisé pour l'acces au web interface de Php83-apache

# Mailpit | Mailpit
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
MP_SMTP_PORT=1025    # Port that will be used for Mailpit | Port qui sera utilisé pour Mailpit
MP_HTTP_PORT=9002    # Port that will be used for access to Mailpit web interface | Port qui sera utilisé pour l'acces au web interface de Mailpit
MP_MAX_MESSAGES=5000 # Maximum number of messages that can be stored in the database | Nombre maximum de message qui peuvent être stockés dans la base de données

##################
###< VARIABLES ###
##################

###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
#
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
DATABASE_URL="mysql://<img src="https://render.githubusercontent.com/render/math?math=\bbox[%230d1117]{\color{white}{%7BMYSQL_USER%7D%3A}}" />{MYSQL_PASSWORD}@<img src="https://render.githubusercontent.com/render/math?math=\bbox[%230d1117]{\color{white}{%7BMYSQL_HOST%7D%3A}}" />{MYSQL_PORT}/db-<img src="https://render.githubusercontent.com/render/math?math=\bbox[%230d1117]{\color{white}{%7BPROJECT_NAME%7D%22}}" />
DATABASE_URL="mysql://root:<img src="https://render.githubusercontent.com/render/math?math=\bbox[%230d1117]{\color{white}{%7BMYSQL_ROOT_PASSWORD%7D%40}}" />{MYSQL_HOST}:<img src="https://render.githubusercontent.com/render/math?math=\bbox[%230d1117]{\color{white}{%7BMYSQL_PORT%7D%2Fdb-}}" />{PROJECT_NAME}"
# DATABASE_URL="mysql://app:!ChangeMe!@127.0.0.1:3306/app?serverVersion=10.11.2-MariaDB&charset=utf8mb4"
# DATABASE_URL="postgresql://app:!ChangeMe!@127.0.0.1:5432/app?serverVersion=16&charset=utf8"
###< doctrine/doctrine-bundle ###

###> symfony/mailer ###
MAILER_DSN=smtp://mailpit
###< symfony/mailer ###
```

### Fichier `.gitignore.NEW`

Le fichier `.gitignore.NEW` contient les chemins de fichiers et d'éléments ignorés par Git.

### Fichier `compose.yaml.NEW`

Le fichier `compose.yaml.NEW` contient la configuration des conteneurs Docker.

### Fichier `Makefile`

Le fichier `Makefile` contient les commandes fréquemment utilisées.

La commande `make help` permet de voir la liste des commandes disponibles.

### Fichier `rector.php`

Le fichier `rector.php` contient la configuration de Rector.

```php
<?php

namespace App;

use Rector\Config\RectorConfig;
use Rector\TypeDeclaration\Rector\Property\TypedPropertyFromStrictConstructorRector;

return RectorConfig::configure()
    ->withPaths([__DIR__ . '/src', __DIR__ . '/tests'])
    ->withIndent(indentChar: ' ', indentSize: 4)
    ->withAttributesSets(symfony: true, doctrine: true)
    ->withRules([
        TypedPropertyFromStrictConstructorRector::class
        ])
    ->withPreparedSets(
        deadCode: true,
        codeQuality: true,
        codingStyle: true,
        typeDeclarations: true,
        privatization: true,
        naming: true,
        instanceOf: true,
        earlyReturn: true,
        strictBooleans: true
    );
```

## Contributions

Si vous souhaitser contribuer à ce dépot, merci de suivre les [instructions de contribution](./CONTRIBUTING.md)

## Licence

Ce projet est sous la Licence [MIT](./LICENSE.md).

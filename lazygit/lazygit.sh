#!/bin/bash

# Colors
info='\033[0;34m'
green='\033[32m'
red='\033[31m'
yellow='\033[33m'
bold='\e[1m'
reset='\033[0m'
success=${green}${bold}✅
error=${red}${bold}❌
warning=${yellow}${bold}⚠

# Install lazygit
cd ~
LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')
curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"
tar xf lazygit.tar.gz lazygit
rm -R lazygit.tar.gz
sudo install ~/lazygit /usr/local/bin
echo ""
if [ ${?} -ne 0 ]; then
    echo -e "${error} Lazygit not installed${reset}"
    exit 1
else
    echo -e "${success} Lazygit installed${reset}"
    echo ""
    echo -e "Run '${info}lazygit${reset}' to start"
    echo ""
    echo -e "Run '${info}lazygit --help${reset}' for more info"
    echo ""
    echo -e "Run '${info}lazygit --version${reset}' to check version"
    echo ""
fi

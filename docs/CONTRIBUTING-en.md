## Contributing Base Symfony 7

[Version française ici](./CONTRIBUTING.md)

<!-- @import "[TOC]" {cmd="toc" depthFrom=3 depthTo=6 orderedList=false} -->

### Table of contents

<!-- code_chunk_output -->

- [Table of contents](#table-of-contents)
- [How to Contribute](#how-to-contribute)
- [Contribution Guidelines](#contribution-guidelines)
- [Submission Process](#submission-process)
- [Coding Style Guidelines](#coding-style-guidelines)
- [Contribute Even More](#contribute-even-more)
- [Roles and Responsibilities](#roles-and-responsibilities)
- [License](#license)
- [Respecting Other Contributors](#respecting-other-contributors)

<!-- /code_chunk_output -->

We encourage you to contribute to this project by following these guidelines.

### How to Contribute

1. **Create a GitHub account if you don't have one already.**
2. **Fork this repository.**
3. **Make your changes.**
4. **Push your changes to your fork.**
5. **Create a merge request (MR) from your fork to the main repository.**
6. **Wait for the project maintainers to review your MR.**
7. **Respond to comments and make any requested changes.**
8. **Your MR will be merged into the main repository once approved.**

### Contribution Guidelines

- Use descriptive and meaningful commit messages.
- Follow the PSR-2 coding standards.
- Write unit tests for your changes.
- Document your code clearly.
- Respect other contributors.

### Submission Process

1. **Create a branch for each new feature or bug fix.**
2. **Commit your changes atomically.**
3. **Push your branch to your fork.**
4. **Create a MR from your branch to the main repository.**
5. **Give your MR a clear title and a detailed description.**
6. **Respond to comments and make any requested changes.**
7. **Your MR will be merged into the main repository once approved.**

### Coding Style Guidelines

- Follow the PSR-2 coding standards.
- Use 4 spaces for indentation.
- Use descriptive variable and function names.
- Add comments to your code to explain its purpose.

### Contribute Even More

- Report bugs and suggest improvements in the issue tracker.
- Help other contributors by answering questions on the forum or chat.
- Document the project by improving the README.md and other documentation.
- Translate the project into other languages.

### Roles and Responsibilities

- **Maintainers**: The project maintainers are responsible for merging merge requests, releasing versions, and guiding the direction of the project.
- **Contributors**: Contributors are developers who submit merge requests to add features, fix bugs, and improve the project.
- **Testers**: Testers help to ensure the quality of the project by reporting bugs and testing new features.

### License

This project is licensed under the [MIT license](../LICENSE.md). See the LICENSE file for more details.

### Respecting Other Contributors

We expect all contributors to be respectful and courteous to each other. Comments and discussions should be constructive and professional.

Thank you for your interest in this project and your contribution!

# Symfony 7 Base

[Version française ici](../README.md)

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=false} -->

## Table of contents

<!-- code_chunk_output -->

- [Table of contents](#table-of-contents)
- [Description](#description)
  - [Folders](#folders)
  - [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Docker containers](#docker-containers)
- [Detailed files in the `COPY_THIS` folder](#detailed-files-in-the-copy_this-folder)
  - [`docker` folder](#docker-folder)
    - [`docker/apache` folder](#dockerapache-folder)
    - [`docker/php83` folder](#dockerphp83-folder)
    - [`docker/phpma` folder](#dockerphpma-folder)
  - [`.env.NEW` file](#envnew-file)
  - [`.gitignore.NEW` file](#gitignorenew-file)
  - [`Makefile` file](#makefile-file)
  - [`compose.yaml.NEW` file](#composeyamlnew-file)
- [Contributing](#contributing)
- [License](#license)

<!-- /code_chunk_output -->

## Description

This repository provides a set of basic configuration files to help you quickly start your Symfony 7 project. It includes:

- A `docker` folder containing Docker files for running your project in containers with Docker.
- An `.env.NEW` file containing example environment variables.
- A `.gitignore.NEW` file to configure Git ignore rules.
- A `compose.yaml.NEW` file to configure and run containers with Docker Compose.
- A `Makefile` to simplify common tasks.

> NOTE: The PHP image in this repository **DOES NOT CONTAIN nodejs OR npm** because Symfony 7 recommends using the [Asset Mapper](https://symfony.com/doc/current/frontend.html#frontend-asset-mapper).

### Folders

This repository contains:

```
📂COPY_THIS
┃ ┣ 📂docker
┃ ┃ ┣ 📂apache
┃ ┃ ┃ ┗ 📜default.conf
┃ ┃ ┣ 📂php83
┃ ┃ ┃ ┗ 📜Dockerfile
┃ ┃ ┗ 📂phpma
┃ ┃ ┃ ┗ 📜Dockerfile
┃ ┣ 📜.env.NEW
┃ ┣ 📜.gitignore.NEW
┃ ┣ 📜compose.yaml.NEW
┃ ┗ 📜Makefile
┣ 📂docs
┃ ┣ 📜CONTRIBUTING-en
┃ ┗ 📜README-en.md
┣ 📜CONTRIBUTING
┣ 📜LICENSE
┗ 📜README.md
```

### Prerequisites

- PHP >= 8.2 (PHP 8.3 is included in this repository)
- [Docker](https://docs.docker.com/get-docker/)
- [Composer](https://getcomposer.org/download/)
- [Symfony CLI](https://symfony.com/download/)
- [Git](https://git-scm.com/downloads)

## Installation

1. Create a new Symfony project.

   - With Symfony CLI:

     - For a webapp:

     ```bash
     symfony new <PROJECT_NAME> -webapp
     ```

     - For a microservice:

     ```bash
     symfony new <PROJECT_NAME>
     ```

2. Download [entire repository](https://gitlab.com/tidjee/base-symfony-7/-/archive/main/base-symfony-7-main.zip) or [the `COPY_THIS` folder](https://gitlab.com/tidjee/base-symfony-7/-/archive/main/base-symfony-7-main.zip?path=COPY_THIS) and unzip it.

3. Paste the contents of the `COPY_THIS` folder into the newly created project.

4. Adapt the `.env` file to your needs with `.env.NEW`.

5. Adapt the `.gitignore` file to your needs with `.gitignore.NEW`.

6. Adapt the `compose.yaml` file to your needs with `compose.yaml.NEW`.

7. Verify the following files:

   - `docker/php83/Dockerfile`
   - `docker/apache/default.conf`
   - `docker/phpma/Dockerfile`

8. Run `docker-compose up -d` to create and start the containers.

9. Link this project to a remote GitLab repository.

   - Store your GitLab credentials locally (**_OPTIONAL_**):

     ```bash
     make git-credentials
     ```

     > The next time you enter your credentials, they will be store in `.my-credentials`.

   - Add this project to a remote GitLab repository:

     ```bash
           git remote add origin <REPOSITORY_URL>.git
     ```

     > Ex: `git remote add origin https://gitlab.com/tidjee/base-symfony-7.git`

     ```bash
     git branch -M main
     ```

     ```bash
     git fetch
     ```

     ```bash
     git branch --set-upstream-to=origin/main main
     ```

     ```bash
     git pull --allow-unrelated-histories
     ```

   - Commit and push your changes:

     ```bash
     git add .
     git commit -m "📦 NEW: Add Symfony project"
     git push
     ```

## Docker containers

- **PHP 8.3-apache**:
  Based on the [php:8.3-apache](https://hub.docker.com/_/php/) image, customized for Symfony 7.
- **MySQL**
  Based on the [mysql:latest](https://hub.docker.com/_/mysql/) image.
- **Adminer**
  Based on the [adminer:latest](https://hub.docker.com/_/adminer/) image.
- **PhpMyAdmin**
  Based on the [phpmyadmin/phpmyadmin:latest](https://hub.docker.com/_/phpmyadmin/) image. I added the `BooDark` theme (a dark Bootstrap theme).
- **Mailpit**
  Mail server (SMTP and HTTP) based on the [axllent/mailpit:latest](https://hub.docker.com/r/axllent/mailpit/) image.

## Detailed files in the `COPY_THIS` folder

### `docker` folder

The `docker` folder contains files for running your project in containers with Docker.

#### `docker/apache` folder

The `docker/apache` folder contains a `default.conf` file. This file is used by Apache to configure the web server.

```xml
<VirtualHost *:80>
    DocumentRoot /var/www/public
    DirectoryIndex index.php

    ServerName localhost

    <Directory /var/www/public>
        AllowOverride None
        Order Allow,Deny
        Allow from All

        FallbackResource /index.php
    </Directory>

    # uncomment the following lines if you install assets as symlinks
    # or run into problems when compiling LESS/Sass/CoffeeScript assets
    # <Directory /var/www/project>
    #     Options FollowSymlinks
    # </Directory>

    # optionally disable the fallback resource for the asset directories
    # which will allow Apache to return a 404 error when files are
    # not found instead of passing the request to Symfony
    <Directory /var/www/public/bundles>
        FallbackResource disabled
    </Directory>
    ErrorLog /var/log/apache2/project_error.log
    CustomLog /var/log/apache2/project_access.log combined
</VirtualHost>
```

#### `docker/php83` folder

The `docker/php83` folder contains a `Dockerfile` file. This file is used to build the PHP image.

```dockerfile
FROM php:8.3-apache

RUN apt-get update \
    &&  apt-get install -y --no-install-recommends \
    locales apt-utils git libicu-dev g++ libpng-dev libxml2-dev libzip-dev libonig-dev libxslt-dev unzip lsd

RUN echo "alias cls='clear'" >> ~/.bashrc \
    echo "alias ls='lsd -al --group-dirs first'" >> ~/.bashrc

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen  \
    &&  echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen \
    &&  locale-gen

RUN curl -sS https://getcomposer.org/installer | php -- \
    &&  mv composer.phar /usr/local/bin/composer

RUN curl -sS https://get.symfony.com/cli/installer | bash \
    &&  mv /root/.symfony5/bin/symfony /usr/local/bin

RUN  docker-php-ext-configure intl

RUN  docker-php-ext-install pdo pdo_mysql opcache intl zip calendar dom mbstring gd xsl \
    &&  pecl install apcu && docker-php-ext-enable apcu

WORKDIR /var/www

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony5/bin/symfony /usr/local/bin/symfony

RUN git config --global user.email "pinet.donatien@gmail.com"
RUN git config --global user.name "tidjee"
```

#### `docker/phpma` folder

The `docker/phpma` folder contains a `Dockerfile` file. This file is used to build the PHPMyAdmin image.

```dockerfile
FROM phpmyadmin/phpmyadmin:latest

RUN apt-get update && apt-get install -y unzip lsd

RUN echo "alias cls='clear'" >> ~/.bashrc \
    echo "alias ls='lsd -al --group-dirs first'" >> ~/.bashrc

WORKDIR /var/www/html

RUN mkdir -p /var/www/html/themes

# Add of the `BooDark` theme | Ajout du theme `BooDark`
RUN curl -L https://files.phpmyadmin.net/themes/boodark/1.1.0/boodark-1.1.0.zip > /tmp/boodark-1.1.0.zip && \
    unzip /tmp/boodark-1.1.0.zip -d /tmp/boodark-1.1.0/ && rm /tmp/boodark-1.1.0.zip && \
    mv /tmp/boodark-1.1.0/boodark /var/www/html/themes/
```

### `.env.NEW` file

The `.env.NEW` file contains the environment variables for your project.

```
##################
###> VARIABLES ###
##################

# These variables are used by compose.yaml | Ces variables sont utilisées par compose.yaml

################################################################################
# !    UPDATE THE VARIABLES IN THIS SECTION BEFORE `docker-compose up -d`      #
#                                ---------------                               #
# ! METS A JOUR LES VARIABLES DANS CETTE SECTION AVANT `docker-compose up -d`  #
################################################################################

# Name of your project | Nom de votre projet
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# TODO CHANGE THE NAME FOR EACH NEW PROJECT | CHANGE LE NOM DE VOTRE PROJET POUR CHAQUE NOUVEAU PROJET
# ? OR DEFAULT NAME `app` | OU NOM PAR DEFAUT `app`
PROJECT_NAME=app

# Database | Base de données
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
MYSQL_PORT=3306          # Port that will be used for MySQL | Port qui sera utilisé pour MySQL
MYSQL_HOST=mysql         # Host that will be used for MySQL | Host qui sera utilisé pour MySQL
MYSQL_ROOT_PASSWORD=toor # Root password | Mot de passe Root
MYSQL_USER=dev           # Development user unsername | Nom de l'utilisateur de developpement
MYSQL_PASSWORD=dev       # Development user password | Mot de passe de l'utilisateur de developpement

# Adminer | Adminer
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
ADMINER_PORT=9001    # Port that will be used for Adminer | Port qui sera utilisé pour Adminer
ADMINER_DESIGN=hydra # Design that will be used for Adminer | Design qui sera utilisé pour Adminer

# Php83-apache | Php83-apache
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
PHP_HTTP_PORT=9000 # Port that will be used for access to Php83-apache web interface | Port qui sera utilisé pour l'acces au web interface de Php83-apache

# Mailpit | Mailpit
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
MP_SMTP_PORT=1025    # Port that will be used for Mailpit | Port qui sera utilisé pour Mailpit
MP_HTTP_PORT=9002    # Port that will be used for access to Mailpit web interface | Port qui sera utilisé pour l'acces au web interface de Mailpit
MP_MAX_MESSAGES=5000 # Maximum number of messages that can be stored in the database | Nombre maximum de message qui peuvent être stockés dans la base de données

##################
###< VARIABLES ###
##################

###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
#
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
DATABASE_URL="mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@${MYSQL_HOST}:${MYSQL_PORT}/db-${PROJECT_NAME}"
DATABASE_URL="mysql://root:${MYSQL_ROOT_PASSWORD}@${MYSQL_HOST}:${MYSQL_PORT}/db-${PROJECT_NAME}"
# DATABASE_URL="mysql://app:!ChangeMe!@127.0.0.1:3306/app?serverVersion=10.11.2-MariaDB&charset=utf8mb4"
# DATABASE_URL="postgresql://app:!ChangeMe!@127.0.0.1:5432/app?serverVersion=16&charset=utf8"
###< doctrine/doctrine-bundle ###

###> symfony/mailer ###
MAILER_DSN=smtp://mailpit
###< symfony/mailer ###
```

### `.gitignore.NEW` file

The `.gitignore.NEW` file contains paths to the gitignore file for your project.

### `Makefile` file

The `Makefile` file contains the most used commands.

The `make help` command will list the available commands.

### `compose.yaml.NEW` file

The `compose.yaml.NEW` file contains the docker-compose configuration for your project.

## Contributing

If you want to contribute to this project, please read the [contributing guide](./CONTRIBUTING-en.md) first.

## License

This project is licensed under the [MIT license](../LICENSE.md).
